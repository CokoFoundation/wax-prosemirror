# Wax-ProseMirror  

This application is being developed by the [Coko Foundation](https://coko.foundation/).

For more information, visit our [chat channel](https://mattermost.coko.foundation/coko/channels/editoria).  

# RoadMap

* Create a highlighter to also test all configurable options

* Create an AddMenu funcionality that will be part of the Configurator eventually

## How to use
```sh
npm i wax-prose-mirror
```

## Default Implemantation

Wax editor build on top of Prosemirror with react integration

```javascript
import React, { Component } from "react";
import { Wax } from "wax-prose-mirror"

class Default extends Component {
  render() {
    return (
      <Wax
        value={"<p> A paragraph </p>"}
      />
    );
  }
} 
```
 the above example will create the default editor, with a top horizontal toolbar with all the available tools 

## Further Configuration

### Styling
Wax comes with prebuild containers with their respective css classes that position the editor, which can be easily overridden using styled components.

### Editor Props

```javascript
autoFocus // sets cursor in the begging of the document
onChange // when the editor's surface is updated (perform an action ex. save)
value // the actual HTML content of the editor
fileUpload // used for uploading images (should return a promise with the actual file path)
placeholder // a placeholder used for empty documents
options // extend Wax schema and plugins
readonly // editor in in read-only mode
onBlur // on focus lost
renderLayout // used to create your own Layout using React components
```

### Fully Configurable Example
```javascript
import React, { Component } from "react";
import styled from "styled-components";

import { Wax, schema, plugins , MainMenuBar} from "wax-prose-mirror"


const WaxContainer = styled.div`
  ${"" /* style override (ex xpub usecase)*/};
  top: 10%;
  height: 90%; 
`;

const MenuBarStyled = styled(MainMenuBar)`
  ${"" /* main menu style override*/};
`;

const MainEditor = styled.div`
  ${"" /* style override*/};
`;

// add your customPlugins
const customPlugins = [];
plugins.push(...customPlugins);

const options = {
  schema,
  plugins
};

const fileUpload = file => {
  // returns a promise with the
  //actual file path
};

const onChange = () => {
  console.log("configurable on change");
};

const onBlur = () => {
};

const menuItems = [
  "undo",
  "redo",
  "strong",
  "image",
  "table",
  "tableDropDownOptions"
]; // A list of some of the available tools we 
// want to appear on the main Toolbar. (full list:https://gitlab.coko.foundation/wax/wax-prosemirror/blob/master/src/config/DefaultMenuItems.js)

class Configurable extends Component {
  render() {
    return (
      <WaxContainer className="wax-container">
        <Wax
          autoFocus
          value={""}
          options={options}
          fileUpload={fileUpload}
          onChange={onChange}
          readonly
          onBlur = {onBlur}
          placeholder={"Type Something..."}
          renderLayout={({ editor, ...props }) => (
            <React.Fragment>
              <MenuBarStyled
                className="main-toolbar"
                menuItems={menuItems}
                {...props}
              />
              <MainEditor className="main-editor">{editor}</MainEditor>
            </React.Fragment>
          )}
        />
      </WaxContainer>
    );
  }
}

export default Configurable;

```
