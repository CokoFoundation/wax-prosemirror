const defaultMenuItems = [
  "undo",
  "redo",
  "strong",
  "em",
  "subscript",
  "superscript",
  "underline",
  "strikethrough",
  "link",
  "plain",
  "ordered_list",
  "bullet_list",
  "lift",
  "join_up",
  "link",
  "image",
  "table",
  "tableDropDownOptions",
  "headings"
];

export default defaultMenuItems;
