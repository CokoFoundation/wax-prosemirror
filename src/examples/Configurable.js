import React, { Component } from "react";
import styled from "styled-components";

import Wax from "../Wax";
import WaxSchema from "../config/classes/WaxSchema";
import WaxKeys from "../config/classes/WaxKeys";

import plugins from "../config/plugins";
import MainMenuBar from "../components/menu/MainMenuBar";

import {
  tableNodes,
  columnResizing,
  tableEditing,
  goToNextCell
} from "prosemirror-tables";

const WaxContainer = styled.div`
  ${"" /* style override*/};
`;

const MenuBarStyled = styled(MainMenuBar)`
  ${"" /* main menu style override*/};
`;

const MainEditor = styled.div`
  ${"" /* style override*/};
`;

// add your customPlugins

// Extend the Schema
const config = {
  nodes: tableNodes({
    tableGroup: "block",
    cellContent: "block+"
  }),
  marks: {}
};

const schema = WaxSchema(config);
// const schema = WaxSchema();

const shortCuts = {
  Tab: goToNextCell(1),
  "Shift-Tab": goToNextCell(-1)
};

const keys = new WaxKeys({ schema: schema, shortCuts: shortCuts });

const customPlugins = [columnResizing(), tableEditing()];
plugins.push(...customPlugins);

const options = {
  schema,
  plugins,
  keys
};

const fileUpload = file => {
  // returns a promise with the
  //actual file path
};

const onChange = () => {
  console.log("configurable on change");
};

const onBlur = () => {
  console.log("on Blur");
};

const menuItems = [
  "undo",
  "redo",
  "strong",
  "image",
  "table",
  "tableDropDownOptions"
];

class Configurable extends Component {
  render() {
    return (
      <WaxContainer className="wax-container">
        <Wax
          autoFocus
          onChange={values => true}
          // readonly
          // options={options}
          onBlur={onBlur}
          value={""}
          fileUpload={fileUpload}
          placeholder={"Type Something..."}
          renderLayout={({ editor, ...props }) => (
            <React.Fragment>
              <MenuBarStyled
                className="main-toolbar"
                menuItems={menuItems}
                {...props}
              />
              <MainEditor className="main-editor">{editor}</MainEditor>
            </React.Fragment>
          )}
        />
      </WaxContainer>
    );
  }
}

export default Configurable;
