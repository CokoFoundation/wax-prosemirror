export { default as Wax } from "./Wax";

export { default as WaxSchema } from "./config/classes/schema";
export { default as plugins } from "./config/plugins";

//Components
export { default as MainMenuBar } from "./components/menu/MainMenuBar";
export { default as TableDropDown } from "./components/menu/TableDropDown";
export { default as ImageUpload } from "./components/menu/ImageUpload";
export {
  default as HeadingsDropDown
} from "./components/menu/HeadingsDropDown";
